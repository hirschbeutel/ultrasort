# -*- coding: utf-8 -*-

### imports ###################################################################
import os

### import from ###############################################################
from ultrasort.tag_reader import TagReader

###############################################################################                    
if __name__ == '__main__':
    data_dir = 'data'
    filename = 'clamped.ttf'
    
    fullfile = os.path.join(data_dir, filename)
    
    reader = TagReader(fullfile)
    reader.read_IFH()
    reader.read_IFD()
    reader.read_strings()
    reader.read_doubles()
    reader.read_arrays()

    print('Filename:', filename)    
    print('Identifier:', reader.identifier)
    print('Version:', reader.version)
    print('IFD Offset:', reader.ifd_offset)

    print('no. of tags:', reader.num_dir_entries)
    print()

    fmt = "%18s %6s %4s %4s %5s %11s %54s"
    print(fmt % ('name', 'ID_hex', 'ID', 'type', 'count', 'offset', 'value'))
    print(108*"=")
    
    for tag in reader.tag_list:
        print(tag)

    reader.iter_IFD()
    
    ### read compressed data ##################################################        
    id = 8154
    tag = reader.get_tag(id)

    for i in range(12):
        i0 = i * 4
        i1 = i * 4 + 4
        
        chunk = tag.array[i0:i1]
        chunk_list = [hex(c) for c in chunk]
        fmt = "%4s %4s %4s %4s"
        chunk = (chunk_list[0], chunk_list[1], chunk_list[2], chunk_list[3])
        print(fmt % chunk)

