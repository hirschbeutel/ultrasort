# -*- coding: utf-8 -*-

### imports ###################################################################
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd

### imports from ##############################################################
from ultrasort.ttf_reader import TTF_Reader

###############################################################################
if __name__ == '__main__':
    data_dir = 'data'
    csv_filename = 'clamped.csv'
    ttf_filename = 'clamped.ttf'
    # N = 146 # compare data shape up to this point
    N = 9000

    ### DON'T EDIT BEYOND THIS LINE ###########################################

    ### read CSV file
    fullfile = os.path.join(data_dir, csv_filename)
    df = pd.read_csv(fullfile, header=None, skiprows=35)
    M = df.values
    csv_data = M.flatten()
    shape = M.shape

    index_tuple = np.where(np.isfinite(M))
    flat_indices = list(np.ravel_multi_index(index_tuple, shape))

    fullfile = os.path.join(data_dir, ttf_filename)
    ttf = TTF_Reader(fullfile, csv_data=csv_data)
    ttf.read_idf()
    ttf.read_data(N)
    N_finite = len(ttf.flat_indices)

    print('CSV:', flat_indices[:N_finite])
    print('TTF:', ttf.flat_indices[:N_finite])
    
    isEqual = flat_indices[:N_finite] == ttf.flat_indices[:N_finite]

    print('Compare first %i datapoints:' % N_finite)
    print('CSV == TTF: %r' % isEqual)
    
    ### plots #################################################################    
    plt.close('all')
    fig, ax = plt.subplots(1, 2, sharex=True, sharey=True)
    ax[0].imshow(M, interpolation=None, vmin=-2, vmax=2)
    ax[1].imshow(ttf.data, interpolation=None, vmin=-2, vmax=2)
    
    fig.savefig('output/clamped.png', dpi=300, transparent=True)    

    fig, ax = plt.subplots()
    ax.imshow(ttf.data - M)
