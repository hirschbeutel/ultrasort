# -*- coding: utf-8 -*-

### imports ###################################################################
import numpy as np
import struct

### relative imports from #####################################################
from .tag import Tag

###############################################################################
class TagReader:
    double_size = 8
    dword_size = 4
    word_size = 2
    
    def __init__(self, filename):
        self.tag_list = []
        self.tag_string_dict = {}
        
        with open(filename, 'rb') as f:
                    self.data = f.read()

    def get_tag(self, id):
        tag = None
        
        for tag in self.tag_list:
            if tag.id == id:
                break
            
        return tag
    
    def iter_IFD(self):
        i0 = self.ifd_offset + 2
        i1 = i0 + self.num_dir_entries * 12
        chunk = self.data[i0:i1]
        
        # N = len(chunk)

        ifd = struct.iter_unpack('hhii', chunk)
        
        for i in ifd:
            print(i)
            
    def read_IFD(self):
        '''
        typedef struct _TifIfd
            {
            	WORD    NumDirEntries;    /* Number of Tags in IFD  */
            	TIFTAG  TagList[];        /* Array of Tags  */
            	DWORD   NextIFDOffset;    /* Offset to next IFD  */
            } TIFIFD;
        '''

        self.i0 = self.ifd_offset
        self.i1 = self.i0 + self.word_size
        chunk = self.data[self.i0:self.i1]
        self.num_dir_entries = struct.unpack('<h', chunk)[0]

        for i in range(self.num_dir_entries):
            tag_entry = self.read_tag()
            tag = Tag(tag_entry)
            self.tag_list.append(tag)

    def read_IFH(self):
        '''
        typedef struct _TiffHeader
            {
            	WORD  Identifier;  /* Byte-order Identifier */
            	WORD  Version;     /* TIFF version number (always 2Ah) */
            	DWORD IFDOffset;   /* Offset of the 1st Image File Directory*/
            } TIFHEAD;
        '''

        self.identifier = struct.unpack(
                '2s', self.data[0:2])[0].decode("utf-8")
        
        self.version = struct.unpack('<h', self.data[2:4])[0]
        
        self.ifd_offset = struct.unpack('<i', self.data[4:8])[0]
        
    def read_tag(self):
        offset = self.i1
        id = self.read_word()
        datatype = self.read_word()
        datacount = self.read_dword()
        value = None
        
        if datatype == 2 and datacount <= 3:
            value = self.read_string(datacount)
            dataoffset = self.read_dword()
        else:
            dataoffset = self.read_dword()

        return (offset, id, datatype, datacount, dataoffset, value)

    def read_dword(self):
        self.i0 = self.i1
        self.i1 = self.i0 + self.dword_size

        chunk = self.data[self.i0:self.i1]
        dword = struct.unpack('<i', chunk)[0]
        
        return dword

    def read_word(self):
        self.i0 = self.i1
        self.i1 = self.i0 + self.word_size

        chunk = self.data[self.i0:self.i1]
        word = struct.unpack('<h', chunk)[0]
        
        return word

    def read_string(self, datacount):
        fmt = str(datacount) + 's'
        i0 = self.i1
        i1 = i0 + datacount
        chunk = self.data[i0:i1]
        
        result = struct.unpack(fmt, chunk)[0].decode("utf-8")

        return result
    
    def read_strings(self):
        for tag in self.tag_list:
            if tag.datatype == 2:
                if tag.datacount <= 1:
                    tag.value = ''
                elif tag.datacount <= 3:
                    pass
                elif tag.datacount > 3:
                    fmt = str(tag.datacount) + 's'
                    i0 = tag.dataoffset
                    i1 = i0 + tag.datacount
                    chunk = self.data[i0:i1]
                    
                    tag.value = struct \
                        .unpack(fmt, chunk)[0] \
                        .decode("utf-8")[:-1]
                        
    def read_doubles(self):
        for tag in self.tag_list:
            if tag.datatype == 8000 and tag.datacount == 1:
                fmt = '<d'
                i0 = tag.dataoffset
                i1 = i0 + self.double_size
                chunk = self.data[i0:i1]
                tag.value = struct.unpack(fmt, chunk)[0]
                
    def read_arrays(self):
        for tag in self.tag_list:
            if tag.datacount > 1:
                if tag.datatype == 8000:
                    fmt = str(tag.datacount) + 'd'
                    size = tag.datacount * self.double_size
                elif tag.datatype == 4:
                    fmt = str(tag.datacount) + 'i'
                    size = tag.datacount * 4
                else:
                    continue

                i0 = tag.dataoffset
                i1 = i0 + size
                chunk = self.data[i0:i1]

                if tag.id == 8154 and tag.datatype == 4:
                    tag.array = chunk
                else:
                    tag.array = np.array(struct.unpack(fmt, chunk))