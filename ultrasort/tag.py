# -*- coding: utf-8 -*-

###############################################################################
class Tag:
    id = -1
    datatype = -1
    datacount = -1
    dataoffset = -1
    name = ''
    offset = 0
    value = None

    tag_name_dict = {
            254: 'SubFileType',
            271: 'Make',
            272: 'Model',
            305: 'Software',
            8003: 'Time',
            8004: 'Sensitivity u/fr',
            8006: 'xc',
            8007: 'yc',
            8009: 'p/mm',
            8010: 'xt', # Tilt Calibration
            8011: 'yt',
            8016: 'Xsq',
            8017: 'YSq',
            8030: 'Reference',
            8031: 'Outside',
            8032: 'Scale',
            8035: 'lx, ly',
            8045: 'OD',
            8047: 'GG',
            8048: 'Gamp',
            8056: '[0. 0.698 7.136]', # [0.        , 0.69831444, 7.13633336]
            8066: 'wafer number',
            8078: 'Temp',
            8088: 'Threshold',
            8091: 'HeadGap',
            8095: 'FSense',
            8096: 'CAngle',
            8097: 'Outer/Top Diameter',
            8100: 'PartType',
            8106: 'MapID',
            8152: 'x-Size',
            8153: 'y-Size',
            8154: 'compressed data',
            8511: '256 x zero',
    }

    def __init__(self, tag):    
        (
            self.offset,
            self.id,
            self.datatype,
            self.datacount,
            self.dataoffset,
            self.value) = tag
        
        if self.id in Tag.tag_name_dict.keys():
            self.name = Tag.tag_name_dict[self.id]
            
    def __repr__(self):
        tag_entry = (
                self.name, hex(self.id), self.id,
                self.datatype, self.datacount, self.dataoffset)
        
        tag_str = '%18s %6s %4i %4i %5i %11i' % tag_entry
        
        if self.value is not None:
            if type(self.value) == float:
                self.str_value = "%5.1f" % self.value
            else:
                self.str_value = self.value
                
            tag_str = ' '.join((tag_str, self.str_value))
        
        return tag_str