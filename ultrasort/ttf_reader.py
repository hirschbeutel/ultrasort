# -*- coding: utf-8 -*-

### imports ###################################################################
import logging
import numpy as np
import struct

###############################################################################
class TTF_Reader:
    def __init__(self, filename, csv_data):
        self.logger = logging.getLogger(__name__)

        # tag_reader.get_tag(8004).value: 0.3164
        self.delta_x = 0.3164
        self.Nx = self.Ny = 516
        self.shape = (self.Nx, self.Ny)
        self.size = self.Nx * self.Ny

        self.data = np.nan * np.ones(self.shape)
        self.csv_data = csv_data
        
        with open(filename, 'rb') as f:
            self.buffer = f.read()
            
            self.byte_order = self.buffer[:2].decode()
            self.is_little_endian = False
            self.is_tiff = False

            if self.byte_order == 'II':
                self.is_little_endian = True

            if int(self.buffer[2]) == 42:
                self.is_tiff = True

            self.idf_offset = self.get_ushort(4)

            self.idf = {}
            
        # self.read_idf()
            
        # self.read_data()

    def get_ushort(self, offset):
        fmt = '<H'
        size = struct.calcsize(fmt)
        i0 = offset
        i1 = i0 + size 
        value = struct.unpack(fmt, self.buffer[i0:i1])[0]
        return value
    
    def read_idf(self):
        idf_size = self.get_ushort(self.idf_offset)
        self.logger.debug(idf_size)
        
        for i in range(idf_size):
            i0 = self.idf_offset + 2 + 12 * i
            i1 = i0 + 12
            chunk = self.buffer[i0:i1]
            tag, as_type, count, value = struct.unpack('<2H2I', chunk)
        
            self.idf[tag] = (as_type, count, value)

    def read_data(self, size=None):
        if size is None:
            size = self.size
        
        ifd_entry = self.idf[8154]
        count = ifd_entry[1]
        offset = ifd_entry[2]

        i0 = offset
        i1 = i0 + 16
        self.header = struct.unpack('<4I', self.buffer[i0:i1])
        self.logger.debug(self.header)

        i = 0
        i0 = offset + 16
        i1 = i0 + 4
        chunk = 0
        N_bits = 0
        n = 12
        previous_element = None
        self.flat_indices = []

        fmt = '%6s %10s: %7s %5s (%6s), %27s, %7s'

        header = (
                'index', '(row, col)',
                'value', 'int', 'hex', 'remaining chunk', 'csv')

        print(fmt % header)
        print(80 * '=')

        while i < size:
            if N_bits < n:

                chunk = chunk * 2**32
                N_bits += 32
                
                chunk = struct.unpack('<I', self.buffer[i0:i1])[0] + chunk
                fmt = '%s remaining %i bits'
                # print(fmt % (hex(chunk), N_bits))

                i0 += 4
                i1 += 4
           
            m = N_bits - n
            element = chunk // 2**m
            chunk = (chunk % 2**m)

            N_bits = m

            index_tuple = np.unravel_index(i, self.shape)

            if element == 0x800:
                fmt = '%6i (%3i, %3i): %7s %5i (%6s), %9s remaining %2i bits, %7.4f'

                params = (
                        i, *index_tuple,
                        'NaN', element, hex(element), hex(chunk), N_bits, self.csv_data[i])
                
                print(fmt % params)
                n = 16

                self.data[index_tuple] = np.nan
                i += 1

            elif element == 0x801:
                fmt = '%6i %10s: %7s %5i (%6s), %9s remaining %2i bits'

                params = (
                        i, '',
                        'Data', element, hex(element), hex(chunk), N_bits)
                
                print(fmt % params)
                n = 16
                
            elif element == 0x802:
                fmt = '%6i %10s: %7s %5i (%6s), %9s remaining %2i bits'

                params = (i, '',
                          'Data', element, hex(element), hex(chunk), N_bits)

                print(fmt % params)
                n = 12
                
            else:
                if previous_element == 0x800:
                    fmt = '%6i %10s: %7s %5i (%6s), %9s remaining %2i bits'
                    
                    params = (
                            i, '', 'Count',
                            element, hex(element), hex(chunk), N_bits)
                    
                    i += element
                    
                elif previous_element == 0x801:
                    value_int = element
                    value = (value_int - 0x10000) / 0x1000 * self.delta_x

                    fmt = ', '.join(('%6i (%3i, %3i): %7.4f %5i (%6s)',
                                     '%9s remaining %2i bits', '%7.4f'))

                    params = (
                            i, *index_tuple, value,
                            element, hex(element), hex(chunk), N_bits,
                            self.csv_data[i])

                    # index_tuple = np.unravel_index(i, self.shape)
                    self.data[index_tuple] = value
                    self.flat_indices.append(i)
                    i += 1
                else:
                    if element >= 2048:
                        delta = element - 0xfff
                    else:
                        delta = element

                    value_int += delta
                        
                    value = (value_int - 0x10000) / 0x1000 * self.delta_x

                    fmt = ', '.join(('%6i (%3i, %3i): %7.4f %5i (%6s)',
                                     '%9s remaining %2i bits', '%7.4f'))

                    params = (
                            i, *index_tuple, value,
                            element, hex(element), hex(chunk), N_bits,
                            self.csv_data[i]
                    )
                    

                    # index_tuple = np.unravel_index(i, self.shape)
                    self.data[index_tuple] = value
                    self.flat_indices.append(i)
                    i += 1

                print(fmt % params)
                n = 12

            previous_element = element
